= Elasticsearch

*Ansible Playbook* footnote:[Ansible Playbook - https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html]
para implantação e configuração do Elasticsearch (TM) footnote:[Elasticsearch - https://www.elastic.co/pt/elasticsearch].

== Valores padrão

Ajuste os valores exemplos para refletir a necessidade de implementação.

*Valores de exemplo*

.`defaults/main.yml`
[source, yaml]
----
include::defaults/main.yml[]
----

Uma das personizações necessárias é a criação do próprio certificado *Root CA*.
Uma das alternativas para gerar a cadeia de certificados *Root CA* pode ser usando
o comando do Elasticsearch (mas poderia ser outra fonte, como: _opessl_ etc).

Um exemplo de uso do próprio Elasticsearch é o comando a seguir:

.Certificados Root CA (`elastic-stack-ca.zip`)
[source]
----
./bin/elasticsearch-certutil ca --pem \
   --ca-dn "C=BR,ST=DF,L=Brasilia,O=DOTEC,OU=GXSIC,emailAddress=GXSIC@EBC" \
   --pass <SENHA_RootCA>
----

Neste caso, a chave privada será gerada com `SENHA_RootCA`, sendo necessário
armazenamento da senha em cofre para uso futuro como por exemplo na geração dos
certificados de serviços cliente como o *Kibana*.

== Operação da playbook

Antes de tudo, defina a arquitetura do serviço e personalize os _valores padrão_
acima segundo a implementação do cluster que irá fazer. Defina, inclusive, os nós
mestres (*master nodes*), nós de dados (*data nodes*) ou um combinado disso para
a realidade da sua implantação.

Exemplo de execução usando os valores padrões de testes:

[source, bash]
----
$ ansible-playbook --ask-pass -u <USER> playbook.yml -i defaults -i inventory
----

onde:

  - `USER`: será o usuário para conexão e execução da _playbook_

=== Instalação incial

A instalação inicial atualmente precisa ter o valor de `elasticsearch_xpack_security_http_ssl_enabled`
como `false`. Isso se dá devido a necessidade inicializar as senhas de usuários
embarcados antes de configurar o serviço sob TLS/SSL - a depender do certificado
que venha a utilizar. Caso seja um certificado "_webtrust_" isso tende a não ser
necessário.

.`HTTP sob TLS/SSL`
[source, yaml]
----
elasticsearch_xpack_security_http_ssl_enabled: false
----

Após a execução inicial é necessário gerar as senhas de usuários embarcados
(exemplo: elastic, apm_system, kibana, logstash_system, beats_system,
remote_monitoring_user), sendo o usuário *`elastic`* o administrador do ambiente.

Para isso, em um dos nós mestres (*_master nodes_*), execute o comando:

.`elasticsearch-setup-passwords interactive`
[source, bash]
----
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords interactive
----

Continue o processo de definição de senhas e atribua chaves fortes para aprimorar
a segurança do serviço.

.`you would like to continue [y/N] y`
[source, console]
----
Initiating the setup of passwords for reserved users
  elastic,apm_system,kibana,logstash_system,beats_system,remote_monitoring_user.
You will be prompted to enter passwords as the process progresses.
Please confirm that you would like to continue [y/N]y


Enter password for [elastic]:
Reenter password for [elastic]:
Enter password for [apm_system]:
Reenter password for [apm_system]:
Enter password for [kibana]:
Reenter password for [kibana]:
Enter password for [logstash_system]:
Reenter password for [logstash_system]:
Enter password for [beats_system]:
Reenter password for [beats_system]:
Enter password for [remote_monitoring_user]:
Reenter password for [remote_monitoring_user]:
Changed password for user [apm_system]
Changed password for user [kibana]
Changed password for user [logstash_system]
Changed password for user [beats_system]
Changed password for user [remote_monitoring_user]
Changed password for user [elastic]
----

Tenha o cuidado de manter as chaves armazenadas seguramente em um cofre de senhas
como o serviço *skynet.ebc* atual.

=== Instalação final

Após instalação inicial atualize o valor de `elasticsearch_xpack_security_http_ssl_enabled`
para `true`. Desta forma, o acesso ao serviço somente deverá ser feito via _HTTPS_.

.`HTTP sob TLS/SSL`
[source, yaml]
----
elasticsearch_xpack_security_http_ssl_enabled: true
----

Execute a _playbook_ novamente para ativar o acesso _HTTPS_ para acesso ao cluster.
